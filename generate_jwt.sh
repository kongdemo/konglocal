#!/bin/bash

# Generate RSA key pair (adjust key size as needed)
openssl genrsa -out private.pem 4096
openssl rsa -in private.pem -pubout > public.pem

# Create your JWT payload as a JSON file (e.g., payload.json)
# Example: {"iss": "your_issuer", "sub": "your_subject", ...}

# Encode header and payload (replace with actual values)
header_b64=$(echo -n '{"alg":"RS512","typ":"JWT"}' | base64 -w 0)
payload_b64=$(jq -r '. | tojson' payload.json | base64 -w 0)

# Sign the combined data with private key
signature=$(echo -n "$header_b64.$payload_b64" | openssl dgst -sha512 -sign private.pem | base64 -w 0)

# Assemble the final JWT
jwt="$header_b64.$payload_b64.$signature"

echo "Generated JWT: $jwt"

