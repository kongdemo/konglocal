import jwt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

def generate_rsa_keypair():
    """Generate RSA key pair"""
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    public_key = private_key.public_key()
    return private_key, public_key

def generate_jwt_token(payload, private_key, algorithm="RS512"):
    """Generate JWT token using RSA private key"""
    jwt_token = jwt.encode(payload, private_key, algorithm=algorithm)
    return jwt_token

def print_keys(private_key, public_key):
    """Print public and private keys"""
    private_key_pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    public_key_pem = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    print("Public Key:")
    print(public_key_pem.decode("utf-8"))
    print("Private Key:")
    print(private_key_pem.decode("utf-8"))

# Generate RSA key pair
private_key, public_key = generate_rsa_keypair()

# Print public and private keys
print_keys(private_key, public_key)

# Generate JWT token
payload = {"iss": "test"}
jwt_token = generate_jwt_token(payload, private_key)

print("JWT Token:")
print(jwt_token)

